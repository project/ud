<?php

/**
 * @file
 * Registry functions.
 */

/**
 * Return menu items.
 */
function _ud_menu() {
  $items = array();

  $ds_path = drupal_get_path('module', 'ds');

  $build_modes = ds_get_build_modes('ud', TRUE);
  asort($build_modes);
  if (!empty($build_modes)) {

    $items['admin/ds/layout/profile'] = array(
      'title' => 'Profile',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ds_display_overview_form', 'profile', 'full', 'ud'),
      'access arguments' => array('administer content types'),
      'file' => 'includes/ds.display.inc',
      'file path' => $ds_path,
    );

    $weight = 20;
    foreach ($build_modes as $key => $value) {
      $items['admin/ds/layout/profile/'. $key] = array(
        'title' => $value['title'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('ds_display_overview_form', 'profile', "$key", 'ud'),
        'access arguments' => array('administer content types'),
        'type' => $key == 'full' ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK,
        'file' => 'includes/ds.display.inc',
        'file path' => $ds_path,
        'weight' => (isset($value['weight']) && !empty($value['weight'])) ? $value['weight'] : $weight++,
      );
    }
  }
  return $items;
}

/**
 * Return ud build modes.
 */
function _ud_content_build_modes() {
  $build_modes = array(
    'full' => array(
      'title' => t('Full profile'),
      'module' => 'ud',
      'weight' => -1,
    ),
    'block' => array(
      'title' => t('Block'),
      'module' => 'ud',
      'weight' => 1,
    ),
  );

  return $build_modes;
}
